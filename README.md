# Notre projet de Réseau et Télécommunication de Deuxième année

## Prérequis

Nous avons utilisé un serveur avec Cockpit installé dessus afin de créer plusieur serveur grace à KVM et à une WebUI

Plus d'informations sur la page [Installation de l'Hyperviseur et des machines virtuelles](./documentation/FedoreCockpit_Setup.md)

## Plan d'addressage & schéma réseau:

Le serveur LTSP à deux adresse IP car il doit communiqué avec les clients légers, et il sert de Routeur pour acceder au réseau des serveurs virtuelles

```
1 Serveur physique:
Fedora Server: 10.108.32.20/24

4 Serveur Virtuelle:
LTSP : 10.108.32.21/24 && 192.168.67.1/24
LDAP : 10.108.32.22/24
NFS / Intranet : 10.108.32.23/24
Serveur Mail : 10.108.32.24/24

LDAP Domain: gretchen.rt
```

![Schéma Réseau](./ressources/schemanetwork.png)

## Création d'un réseau de client légers sous Linux

[Mise en place du serveur LTSP](documentation/LTSPInstall_Setup.md)

[Configuration des VM Serveurs](documentation/VMServer_Config.md)

[Mise en place du serveur DNS et DHCP](documentation/DNSMASQServer_Config.md)

[Mise en place du serveur LDAP](documentation/LDAPServer_Setup.md)

[Mise en place du serveur NFS](documentation/NFSServer_Setup.md)

[Mise en place du Client x86](documentation/x86Client_Setup.md)

[Mise en place du Client ARM (Raspberry Pi)](documentation/RasperryPiClient_Setup.md)

## Mise en place d'un réseau Intranet

[Installation de l'environnement NGINX](documentation/SERVICES_Base_Install.md)

[Installation et configuration d'un Nextcloud](documentation/SERVICES_Nextcloud_Setup.md)

[Installation et configuration des services de messagerie](documentation/SERVICES_Setup_Mail.md)

## Autre (Hors-Sujet)

[Mise en place d'un serveur NGINX](documentation/NGINXServer_Setup.md)

# Notes

This documentation is provided as-is. We don't owe you anything, but if you want to contribute to this documentation feel free to make a merge request!
