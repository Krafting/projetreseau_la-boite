# Common DHCP mistakes

When installing a LTSP server, most guide wants you to use `isc-dhcp-server` but some other guides; or the official doc.; use `dnsmasq`, so a common mistake is to install both package.

BUT!

Both package have a DHCP server, so if you try to configure one server, while the other is running, you will have issue.

We decided to use `isc-dhcp-server` for the time being, if we encounter issue, this will all be documented here.

WELP! LTSP really like `dnsmasq` so after trying ISC, we used LTSP to configure the DHCP automatically with the command

`ltsp dnsmasq` 

And after that we needed our machines to use the 192.168.67.0/24 Network (as the default setup)

So, don't forget to uninstall the package `isc-dhcp-server` if you go the `dnsmasq` route
