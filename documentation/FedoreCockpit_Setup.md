# Installation de l'Hyperviseur et des machines virtuelles

Comme décrit précédemment, nous allons utiliser Fedora Server en tant que hyperviseur utilisant KVM afin de créer des machines virtuelles sous Ubuntu Server. L’IUT nous a prêté un serveur, que nous avons pu remettre à 0 et ensuite installé Fedora Server.

Une fois Fedora installé, nous allons installer Cockpit, ce qui nous permettra de gérer toutes nos machines virtuelles et nos serveurs via une seule interface Web.

Pour installer Cockpit il nous suffit de faire cette commande

`dnf install cockpit`

Une fois Cockpit installé, nous pouvons accéder à l’URL principale de notre hyperviseur https://10.108.32.20:9090. Mais nous avons eu un problème avec le port par défaut de Cockpit (:9090), que nous avons finalement changé par le port 443 afin que l’on puisse accéder au serveur même via le VPN de l’université. En effet l’université n’autorise que certains ports lorsque l’on est connecté au VPN. Le port 443 étant le port HTTPS il est forcément accepté par le VPN.
