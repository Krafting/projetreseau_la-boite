# Config d'un serveur DNS & DHCP avec le service DNSMasq

# Fichiers utilisé

* /etc/local-dns.conf 
* /etc/forward-dns.conf
* /etc/dnsmasq.conf

# Préambule

Avant toute chose nous devons désactiver le service systemd-resolved

`systemctl disable systemd-resolved && systemctl stop systemd-resolved` 

Note: LTSP créer une configuration qui désactive le service DNS par défaut, il faut changer la ligne "port=0" en "port=53" pour le réactiver.

# Configuration global

Nous avons déjà généré le fichier de configuration DNSMASQ grâce à LTSP. Nous allons juste éditer le fichier généré comme suit. 

Dans le fichier `/etc/dnsmasq.d/ltsp-dnsmasq.conf` voici la configuration que nous allons devoir ajouter pour faire fonctionner le serveur DNS

```
# port=0 disables the DNS service of dnsmasq
port=53

# DNS configuration
domain-needed
bogus-priv

# IP DES FORWARDERS
resolv-file=/etc/forward-dns.hosts
strict-order

# LES ENREGISTREMENTS A ET AAAA
addn-hosts=/etc/local-dns.hosts
expand-hosts
domain=gretchen.rt

# ON ECOUTE SUR CES INTERFACES (Bien changé pour vos IP)
listen-address=127.0.0.1,192.168.67.1,10.108.32.21

# LES LOGS
log-queries
log-dhcp
```

Quelques explications :
- domain-needed : Permet de ne pas transmettre aux forwarders une résolution de nom qui n'est pas pleinement qualifié.
- bogus-priv : Permet de ne pas transmettre aux forwarders les résolutions inverses non trouvées par la configuration locale
- resolv-file : Permet de définir un fichier de configuration différent que /etc/resolv.conf
- strict-order : Permet de respecter l'ordre strict des différents DNS paramétrés dans le fichier resolv.conf
- addn-hosts : Permet de définir un fichier de configuration différent que /etc/hosts pour la résolution des noms
- expand-hosts : Ajoute automatiquement le domaine aux noms simples
- domain : Définit le domaine (suffixe aux noms locaux)
- log-queries : Permet de logguer les requêtes DNS (par défaut dans /var/log/messages) - Très verbeux, utile pour du debug

# Les fichiers de config:

`/etc/local-dns.hosts`: Le ficher hosts DNS

```
### DNS SERVERS ###
127.0.0.1       server-ltsp

10.108.32.21    ltsp server-ltsp
10.108.32.22    ldap
10.108.32.23    others web portainer cockpit-web cloud
10.108.32.24    mail
```

`/etc/forward-dns.hosts`: Le ficher des autres serveurs DNS

```
### LES FORWARDERS ###
nameserver 192.168.67.1
nameserver 1.1.1.1
```

# Reconfiguration du DHCP

On va devoir re-configurer le DHCP pour donner à nos machines le bon serveur DNS (le nôtre), une adresse IP correcte ainsi que la passerelle par défaut (le serveur LTSP lui-même).

Dans le fichier `/etc/dnsmasq.d/ltsp-dnsmasq.conf` il faut modifier cette ligne:

Remplacer `192.168.67.1,1.1.1.1` par les serveurs DNS de votre choix

```
dhcp-option=option:dns-server,192.168.67.1,1.1.1.1
```

On peut aussi ajouter la passerelle par défaut dans la config du DHCP afin que notre serveur LTSP serve aussi de routeur:

```
# Passerelle par default
dhcp-option=option:router,192.168.67.1
```