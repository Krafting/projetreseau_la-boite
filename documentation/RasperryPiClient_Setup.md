# Configuration pour booter un Raspberry Pi

En sachant que le rasperry pi tourne sous une architecture ARM, on ne peut pas réutiliser les images que nous avons créée précédemment dans [x86Client_Setup.md](x86Client_Setup.md) 

Nous devons donc générer une image LTSP adapté à l'architecture. Pour cela nous avons choisi Raspberry Pi OS, l'OS par défaut des rasperry pi.

## Prérequis

Nous allons faire ca sur le serveur LTSP, donc il faut bien penser à faire l'étape [LTSPInstall_Setup.md](LTSPInstall_Setup.md)

Il faut aussi télécharger une image Raspberry Pi OS par la commande suivante : 

`wget https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2022-01-28/2022-01-28-raspios-bullseye-armhf-full.zip`

## Creation de la base

Pour commencer, on décompresse l'image téléchargée

`unzip 2022-01-28-raspios-bullseye-armhf-full.zip`

Ensuite on va se retrouver avec un fichier .img que l'on va définir dans /dev/loop8

`losetup -rP /dev/loop8 2022-01-28-raspios-bullseye-armhf-full.img`

On va monter nos différentes partitions dans /mnt et copier les données dans un dossier sur le serveur, ici `/srv/ltsp/raspios`

```
mount -o ro /dev/loop8p2 /mnt
time cp -a /mnt/. /srv/ltsp/raspios
umount /mnt
mount -o ro /dev/loop8p1 /mnt
cp -a /mnt/. /srv/ltsp/raspios/boot/
umount /mnt
losetup -d /dev/loop8
```

À partir de ce moment, Raspberry Pi OS devrait être dans `/srv/ltsp/raspios`. On va maintenant préparer le système pour qu'il puisse démarrer sur le réseau

Il faut aller dans le dossier `/srv/ltsp/raspios`

`cd /srv/ltsp/raspios`

On enlève les services qu'on ne veut pas lors du netboot

`systemctl mask --root=. dhcpcd dphys-swapfile raspi-config resize2fs_once`

On enlève la carte SD des entrées fstab afin de ne pas démarrer dessus

`echo 'proc     /proc     proc     defaults     0     0' >./etc/fstab`

On utilise une commande approprié pour monté le NFS lors du démarrage (pensez bien à verifier l'@ip du serveur LTSP dans nfsroot=)

```
echo 'ip=dhcp root=/dev/nfs rw nfsroot=192.168.67.1:/srv/ltsp/raspios,vers=3,tcp,nolock console=serial0,115200 console=tty1 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles modprobe.blacklist=bcm2835_v4l2' >./boot/cmdline.txt
```

## Preparation du serveur

On ajoute `RPI_IMAGE` dans la configuration LTSP situé dans `/etc/ltsp/ltsp.conf`

```
[server]
RPI_IMAGE="raspios"
```

Puis on régénere les configurations LTSP avec : 

```
ltsp kernel raspios
ltsp initrd
ltsp nfs
```

## Préparation du Raspberry Pi

Les Raspberry Pi ne peuvent pas boot sur le réseau par défaut. Il faut donc éditer le bootloader. Nous avons utilisé un Raspberry Pi 4. 

On execute la commande depuis Raspberry Pi OS depuis un carte SD

`sudo raspi-config`

dans les menus on sélectionne 

```
Advanced options
    Boot order
        Network boot
```

On peut maintenant éteindre le Raspberry et débrancher la carte SD

## Demarrage réseau en mode écriture

A ce moment, on est prêt à demarrer le Raspberry Pi sur le dossier en mode écriture. Ce qui veut dire que peut importe ce que nous faisons comme par exemple installé de nouveaux programmes seront directement sauvegardé sur `/srv/ltsp/raspios`

On va exporter le fichier en NFS read-write mode 

```
echo '/srv/ltsp/raspios  *(rw,async,crossmnt,no_subtree_check,no_root_squash,insecure)' >/etc/exports.d/ltsp-raspios.exports
exportfs -ra
```

On voudra peut être modifié * avec une @IP pour autorisé l'accès à un client seulement, ou à un réseau.

Maintenant on démarre le Raspberry Pi avec un câble branché sur le réseau et pas de carte SD sinon il bootera sur la carte et on peut installer les paquets désiré.

`sudo apt install --install-recommends ltsp epoptes-client`

Note: Couple user/password par défaut: pi/pi

Note : vous voulez peut être préparer l'os pour se connecter à un LDAP, pour cela réferrez vous à la section [Installation d'un client LDAP sous Raspberry Pi OS](#installation-dun-client-ldap-sous-raspberry-pi-os)

## Démarrage réseau

On édite la commande ligne pour démarrer sur l'image que l'on va créée.

```
echo 'ip=dhcp root=/dev/nfs nfsroot=192.168.67.1:/srv/ltsp/raspios,vers=3,tcp,nolock init=/usr/share/ltsp/client/init/init ltsp.image=images/raspios.img console=serial0,115200 console=tty1 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles modprobe.blacklist=bcm2835_v4l2' >/srv/ltsp/raspios/boot/cmdline.txt
```

Ensuite, on va générer l'image LTSP statique via la commande:

`ltsp image raspios --mksquashfs-params='-comp lzo'`

On retire l'export en NFS crée précédement par question de sécurité:

```
rm /etc/exports.d/ltsp-raspios.exports
exportfs -ra
```

## Modification de l'image de base

Si vous voulez faire des modifications ulterieurement à l'image de base, il va falloir changer la cmdline et remonter le NFS comme cela:

```
echo 'ip=dhcp root=/dev/nfs rw nfsroot=192.168.67.1:/srv/ltsp/raspios,vers=3,tcp,nolock console=serial0,115200 console=tty1 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles modprobe.blacklist=bcm2835_v4l2' >./boot/cmdline.txt
echo '/srv/ltsp/raspios  *(rw,async,crossmnt,no_subtree_check,no_root_squash,insecure)' >/etc/exports.d/ltsp-raspios.exports
exportfs -ra
rm /srv/ltsp/images/raspios.img
```

On peut désormais booter sur le réseau comme dans l'étape [Démarrage réseau en mode écriture](#demarrage-réseau-en-mode-écriture)

Si on veux recréer une image statique, il suffit de suivre l'étape [Démarrage réseau](#démarrage-réseau)

## Installation d'un client LDAP sous Raspberry Pi OS

Pour commencer, on veux modifier l'image de base alors il va falloir démarrer sur l'image dans /srv/ltsp/raspios (Voir étape ci-dessus)

On boot un raspberry sur l'image et on commence a installer les paquets:

`sudo apt-get install libnss-ldapd`

On configure l'IP du serveur LDAP quand demandé, ainsi que le DN de base. puis on vérifie que la config est correct en allant regarder le fichier dans `/etc/nslcd.conf` qui devrais ressembler a ca:

```
...
uri ldap://10.108.32.21/
...
base dc=gretchen,dc=rt
...
```

Ensuite, on va modifier la façon dont les utilisateurs s'authentifient dans le fichier `/etc/nsswitch.conf`

On modifie les 4 premieres lignes comme suit:

```
passwd:         compat ldap
group:          compat ldap
shadow:         compat ldap
gshadow:         compat ldap
```

On va mettre a jour les modules PAM avec la commande

`sudo pam-auth-update`

Vérifiez que "Unix Authentication" et "LDAP Authentication" soit cocher (barre d'espace pour cocher un item), puis faites OK

On active la création des dossiers Home lors de la premiere connexion en ajouter ceci a la fin du fichier `/etc/pam.d/common-session`:

```
session optional pam_mkhomedir.so skel=/etc/skel umask=077
```

On va, pour finir redémarrer des services systèmes (ou redémarrer)

```
sudo service nslcd stop
sudo service nslcd start

sudo service nscd stop
sudo service nscd start
```

Vous pouvez tester la configuration en vous connectant avec un compte sur votre LDAP

```
su - hdana
```

## 2FA avec LDAP

Pour la 2FA nous utilisons `google-authenticator` avec les modules PAM, de ce faite, un fichier de verification des codes de chaque utilisateur est crée dans leur dossiers home respectifs, vu que les dossiers home sont montés en NFS, la 2FA restera persistante peut importe ou on se connecte. 

On installe le paquet requis:

`sudo apt install libpam-google-authenticator`

Ensuite, on edite le fichier `/etc/pam.d/common-auth` et on ajoute la ligne suivante à la fin du fichier

```
auth    required    pam_google_authenticator.so nullok
```

All set! Un utilisateur va pouvoir mettre en place la 2FA pour son compte en ouvrant un Terminal et en tapant la commande `google-authenticator`. Un QR code sera afficher, vous pourrez le scanner avec une applications comme [Secur](https://f-droid.org/fr/packages/com.u2fa.secur/) quelques questions vous sera posé, pour plus de sécurité répondez oui (y) à chaque questions.