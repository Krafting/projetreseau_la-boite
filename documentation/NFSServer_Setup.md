# Installation de NFS et configuration du service NFS

## Installation des paquets

On commence par installer les paquets requis pour créer un serveur NFS

```
apt update && apt install nfs-kernel-server
```

## Creation des dossiers a partager	

On va ensuite créer les dossiers à partager, on partagera un dossier publique et un dossier qui servira de stockage pour les dossiers personnels des utilisateurs

```
mkdir /data && mkdir /data/home && mkdir /data/public
```  

On édite les permissions des dossiers pour avoir accès à la création des dossiers

```
sudo chown nobody:nogroup -R /data
sudo chmod 755 -R /data
```

## Créer le fichier pour exporter les dossiers en NFS

On crée ensuite le fichier qui définit les dossiers à exporter en NFS

```
nano /etc/exports
```

Contenu du fichier /etc/exports:

```
/data/home      *(rw,sync,no_subtree_check) 
/data/public    *(rw,sync,no_root_squash,no_subtree_check)
```

On redémarre le service NFS et les dossiers devraient désormais être accessibles aux clients.

```
sudo systemctl restart nfs-kernel-server
```	

## Autoriser NFS dans le firewall d'ubuntu

Ubuntu utilise un par-feu par défaut, si on veut le laisser actif il faut ajouter une règle à ce par-feu afin d’autoriser les services NFS

```
sudo ufw allow from 10.0.0.0/8 to any port nfs
```

# Configuration des clients LTSP pour le /home sur un serveur NFS

## Editer le fichiers /etc/ltsp/ltsp.conf	

Pour monter automatiquement les dossiers exporté sur le serveur NFS sur les clients on va éditer le fichier de configuration de LTSP dans /etc/ltsp/ltsp.conf

Dans ce fichier, pour les dossiers HOME on va décommenter et éditer la ligne FSTAB_HOME:

```
# 10.108.32.23 : @IP du server NFS
FSTAB_HOME="10.108.32.23:/data/home /home nfs defaults,nolock 0 0"
```

On met à jour les fichiers LTSP:

```
ltsp initrd
```

# Configuration d'un autre dossier monté en NFS

Pour configurer un autre point de montage, il faut, comme précédemment pour le homedir, ajouter d'autres entrée dans le fichier fstab du client légers comme cela:

```
# Utiliser "FSTAB_" en prefix pour definir une entrée fstab
FSTAB_PUBLIC="10.108.32.23:/data/public /mnt/public nfs defaults,nolock 0 0"
```

On met à jour les fichiers LTSP:

```
ltsp initrd
```
