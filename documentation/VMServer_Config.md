# Configuration des machines virtuelles

Nous avons décidé de prendre Ubuntu Server 20.04 pour le système d’exploitation de nos machines virtuelles. Ubuntu server ne fait pas sa configuration comme Debian, il utilise Netplan au lieu de la configuration de base de Debian située dans `/etc/network/interfaces`.

À la place il utilise le fichier situé dans `/etc/netplan/00-installer-config.yml`

Pour le serveur LTSP nous avons cette configuration IP, sur les autres serveurs nous avons la même chose mais avec une seule interface réseau (voir [schéma réseau](../README.md)).

```
network:
	ethernets:
		enp1s0:
			addresses:
			- 10.108.32.20/16
			gateway4: 10.108.255.254
			nameservers:
			  addresses:
			  - 10.100.100.20
			  - 10.100.100.21
			  search: []
		enp7s0:
			addresses:
			- 192.168.67.1/24
			nameservers:
			  addresses:
			  - 192.168.67.1
			  search: []
	version 2
```
