# Installation du serveur LTSP

## Installation des paquets

On ajoute le repo officiel de LTSP:

`add-apt-repository ppa:ltsp`

Ensuite nous installons les paquets requis sur le serveur LTSP:

`apt install ltsp dnmasq ltsp-binaries ipxe net-tools nfs-kernel-server`

## Configuration globale:

Générer le fichier DNSMASQ, qui permettra de créer une configuration DHCP par défaut:

`ltsp dnsmasq --proxy-dhcp=0`

Désactiver le service DNS de ubuntu server, qui provoque un conflit avec notre serveur DNSMASQ:

`systemctl stop systemd-resolved.service && systemctl disable systemd-resolved.service`

Redémarrer DNSMASQ:

`systemctl restart dnsmasq`

Création du fichier de configuration /etc/ltsp/ltsp.conf:

`install -m 0660 -g sudo /usr/share/ltsp/common/ltsp/ltsp.conf /etc/ltsp/ltsp.conf`

On va editer la configuration ltsp situé dans `/etc/ltsp/ltsp.conf` (fichier copier dans la commande précédente)

```
[server]
RPI_IMAGE="raspios"
DEBUG_LOG=1
ADD_IMAGE_EXCLUDES="list.excludes"
PAM_AUTH_TYPE=0
SEARCH_DOMAIN="gretchen.rt"
DNS_SERVER="192.168.67.1 10.108.32.21"

[clients]
FSTAB_HOME="10.108.32.23:/data/home /home nfs defaults,nolock 0 0"
FSTAB_PUBLIC="10.108.32.23:/data/public /mnt/public nfs defaults,nolock 0 0"
```

Explications: 

```
[server]
RPI_IMAGE : Nom de l'image a utiliser pour les raspberry pi
DEBUG_LOG : Niveau de déboggage
ADD_IMAGE_EXCLUDES : Fichiers a exclures lors de la création de l'image
PAM_AUTH_TYPE : Utilise LDAP
SEARCH_DOMAIN : Le nom de domaine LDAP
DNS_SERVER : Les serveurs DNS a configurer sur les clients

[clients]
FSTAB_HOME : Montage des dossiers homes depuis un NFS
FSTAB_PUBLIC : Montage d'un dossier public depuis un NFS
```

Pour la configuration de DNSMasq (Serveur DNS et DHCP) voir la section [Mise en place du serveur DNS et DHCP](DNSMASQServer_Config.md)

# Le rendre routeur

Nous avons décidé que notre serveur LTSP servira de routeur entre le réseau des clients légers et des serveurs qui proposent d'autres services (comme par exemple le serveur NFS ou LDAP) afin de séparer dans du NAT les clients légers.

Pour cela on commence par editer le fichier `/etc/sysctl.conf` en décommentant la ligne:

```
net.ipv4.ip_forward=1
```

Pour recharger la configuration, nous lançons simplement la commande suivante

`sysctl -p`

Ensuite on setup le NAT grâce à iptables, nous faisons un script pour pouvoir recharger les règles (nous pouvons aussi utiliser iptables-persistent, mais nous n'avons décider de ne pas le faire)

Fichier `/home/ltsp/Documents/iptables.rulesv4`:

```
#!/bin/bash

# On flush les règles
iptables -F
iptables -F -t nat

# enp1s0 => Interface Externe
# enp7s0 => interface Interne
# On recréer les règles voulu
iptables -t nat -A POSTROUTING -o enp1s0 -j MASQUERADE
iptables -A FORWARD -i enp7s0 -j ACCEPT
```

Nous pouvons ensuite lancer ce script et notre serveur devrais désormais fonctionner en tant que routeur NAT
