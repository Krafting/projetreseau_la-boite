# Mise en place d'une VM de base sous Ubuntu 20.04

Pour booter une image sur le réseau il nous faut une image de base. Nous avons décider de prendre Ubuntu 20.04 en tant que base et Virtual Box pour créer notre VM de base.

Paramètres du disque Virtual Box:

```
Disque Taille Fixe
Format: VMDK
```

On boot ensuite un ISO et on installe Ubuntu de facons (presque) normal. En effet, par défaut Ubuntu utilise du LVM ce qui ne marche pas bien avec LTSP, on va devoir formatter manuellement notre disque.

```
Partition EFI /boot/efi
Partition / en ext4 
```

Une fois installer, on peut éteindre la VM et mettre le disque `<nomVM>-flat.vmdk` dans notre serveur LTSP 

## Génération de l'image

Pour générer l'image de la VM on fais un lien symbolique comme cela:

`ln -sf "/path/to/<vmname>-flat.vmdk" /srv/ltsp/ubuntu.img`

Puis on lance la commande suivante (Note: 'ubuntu' correspond au nom que l'on a donné au lien symbolique)

`ltsp image ubuntu`

On générère ensuite le menu iPXE 

`ltsp ipxe`

Et on génère le fichier initrd qui permet d'exporter certains dossier du serveur LTSP dans le client LTSP (à refaire à chaque fois que l'on édite le fichier `ltsp.conf`):

`ltsp initrd`

Si on a configuré un serveur NFS pour les /home on génère le fichier export grace à

`ltsp nfs`

# Client LDAP

On démarre la VM de base sous Virtual Box.

## Désactiver la liste des utilisateurs (GDM)

Créer le profile gdm qui contient les lignes suivantes dans `/etc/dconf/profile/gdm`

```
user-db:user
system-db:gdm
file-db:/usr/share/gdm/greeter-dconf-defaults
```

gdm est le nom de la base de donnée dconf

Créer un fichier de clé gdm pour un paramètre dans `/etc/dconf/db/gdm.d/00-login-screen`:

```
[org/gnome/login-screen]
# Do not show the user list
disable-user-list=true
```

On met à jour les bases de données dconf du système

```
dconf update
```

## Installation des paquets requis

```
apt update && apt -y install libnss-ldap libpam-ldap ldap-utils
```

## Setup de LDAP

Lors de l'instalation des paquets, on va vous demander plusieurs informations.

à commencer par l'@ IP du serveur LDAP qui devra être au format:

`ldap://X.X.X.X:389`

Ensuite, pour le DN, mettre le DN du serveur LDAP

`dc=gretchen,dc=rt`

Utiliser la version `3` de LDAP

On séléctionne `Oui` à "Make local root database admin"

On séléctionne `Non` à "Does the LDAP database require login?"

On met ensuite le CN du compte admin de notre LDAP

`cn=admin,dc=gretchen,dc=rt`

On lui donne le mot de passe admin.

On ne chiffre pas les MDP (clear)

Après l'installation, on va editer le fichier `/etc/nsswitch.conf` comme suit:

```
passwd: compat systemd ldap
group: compat systemd ldap
shadow: compat
```

Dans le fichier ` /etc/pam.d/common-password` on retire "use_authtok" comme ceci:

```
password [success=1 user_unknown=ignore default=die] pam_ldap.so try_first_pass
```

On active la création des dossiers Home lors de la premiere connexion en ajouter ceci a la fin du fichier `/etc/pam.d/common-session`:

```
session optional pam_mkhomedir.so skel=/etc/skel umask=077
```

Vous avez configurer correctement votre client LDAP

## Tester la configuration

Vous pouvez tester la configuration en vous connectant avec un compte sur votre LDAP

```
su - hdana
```

## 2FA with LDAP

Pour la 2FA nous utilisons `google-authenticator` avec les modules PAM, de ce faite, un fichier de verification des codes de chaque utilisateur est crée dans leur dossiers home respectifs, vu que les dossiers home sont montés en NFS, la 2FA restera persistante peut importe ou on se connecte. 

On installe le paquet requis:

`sudo apt install libpam-google-authenticator`

Ensuite, on edite le fichier `/etc/pam.d/common-auth` et on ajoute la ligne suivante à la fin du fichier

```
auth    required    pam_google_authenticator.so nullok
```

All set! Un utilisateur va pouvoir mettre en place la 2FA pour son compte en ouvrant un Terminal et en tapant la commande `google-authenticator`. Un QR code sera afficher, vous pourrez le scanner avec une applications comme [Secur](https://f-droid.org/fr/packages/com.u2fa.secur/) quelques questions vous sera posé, pour plus de sécurité répondez oui (y) à chaque questions.


## Regénérer une image

Pour regénerer une image, on doit remettre le <nomvm>-flat.vmdk sur le serveur et refaire l'étape [Génération de l'image](#génération-de-limage)
