# Préparation du système

Tout d'abord on met à jour le système

`sudo apt update && sudo apt upgrade`

On configure le nom de domain de la machine

`sudo hostnamectl set-hostname ldap.gretchen.rt`


# Installation OpenLDAP

On installe les paquets requis pour le service LDAP

`apt install slapd ldap-utils`

On laisse le nom de domain par defaut (doit correspondre à celui configurer précedemment)

Verification de l'installation avec slapcat (ca affiche le contenue du ldap)

`slapcat`

#  Installation d'une Web UI 

On a choisi LDAP Account Manager (LAM)

On télécharge le paquet officiel depuis  sourceforge

`wget https://downloads.sourceforge.net/project/lam/LAM/7.8/ldap-account-manager_7.8-1_all.deb`

On installe le paquet mais les dépendances ne sont pas installé avec par défaut

On run donc la commande `apt -f install` comme ceci : 

``` 
dpkg -i ldap-account-manager_7.8-1_all.deb
apt -f install
dpkg -i ldap-account-manager_7.8-1_all.deb
```

On se connecte avec http://ip_serveur/lam/ , puis dans l'interface "lam configuration" puis dans "edit server profiles" 

Le mot de passe par défaut est lam 

On change les parametres suivant avec le nom de domaine configuré

```
General Settings: 
    Tool settings : 
        Tree suffix : dc=gretchen,dc=rt
    Security settings : 
        List of valid users : cn=admin,dc=gretchen,dc=rt

Account types :
    Active accounts types :
        Users : 
            LDAP Suffix : ou=users,dc=gretchen,dc=rt
        Groups : 
            LDAP Suffix : ou=groups,dc=gretchen,dc=rt
```

Puis on peut se connecter via l'interface Web avec le mot de passe du compte admin setup a l'installation des paquets.

Le LAM va nous demander de créer les schémas de groups et des users (nous acceptons sans discuter)
