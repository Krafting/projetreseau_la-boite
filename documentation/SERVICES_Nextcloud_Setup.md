# Services - Installation et configuration d'un Nextcloud

## Table des matières

- 

## 1. Préparation de l'environnement
Avant de commencer, il est nécéssaire de préparer l'environnement. Nous allons tout d'abord commencer avec l'ajout des enregistrements DNS nécéssaires. 

> Vérifiez que votre poste utilise bien les bons serveurs DNS

Sur notre serveur DNS, on se place dans le dossier `/etc` et on effectue une copie du fichier `/etc/local-dns.hosts`
```
sudo cp local-dns.hosts local-dns.hosts.bak
```
> **ATTENTION** : Cela écrasera votre copie de sauvegarde précédente (si elle existe)

On y ajoute ensuite l'enregistrements requis par le proxy
```
10.108.32.23    others web admin cockpit-web cloud
```

Pensez à bien recharger votre service DNS, ici avec dnsmasq :
```
pkill -HUP dnsmasq 
```

Ensuite, sur notre serveur Docker, nous allons créer des dossiers dont notre conteneur Nextcloud aura besoin afin de stocker sa configuration de maière persistente.

Placez vous dans `/docker-data`, puis créez les 5 dossiers avec la commande suivante :
```
sudo mkdir nextcloud nextcloud/nextcloud nextcloud/apps nextcloud/config nextcloud/data
```

## 2. Installation du conteneur et configuration du proxy
Nous allons ensuite installer notre conteneur Nextcloud et configurer notre proxy afin qu'il soit accéssible via un navigateur.

Notre machine étant maintenant prète, nous allons déployer directement le conteneur avec la commande suivante :
```
docker run -d -p 8001:80 --name nextcloud \
    -v /docker-data/nextcloud/nextcloud:/var/www/html \
    -v /docker-data/nextcloud/apps:/var/www/html/custom_apps \
    -v /docker-data/nextcloud/config:/var/www/html/config \
    -v /docker-data/nextcloud/data:/var/www/html/data \
    nextcloud
```
> Cela déploiera notre conteneur avec le port **8001** comme port Web. 

Ouvrez maintenant l'interface de gestion du proxy (*rappel : port `81`*), connectez-vous si besoin, et accédez à **Hosts > Proxy Hosts**. Cliquez ensuite sur **Add Proxy Host**. Configurez le nouvel hote tel que :
```
Domain Names : cloud.gretchen.rt

Scheme : http
Forward Hostane / IP : 10.108.32.23
Forward Port : 8001

X Cache Assets
✓ Block Common Exploits
X Websockets Support
```

Validez en cliquant sur **Save**.

> **Note**: Pour l'instant, nous n'avons pas d'infrastructure PKI ou de CA permettant d'emmettre un certificat pour le proxy, c'est pourquoi nous passerons en http uniquement pour l'instant.

Vous pouvez ensuite accéder à Nextcloud sur [http://cloud.gretchen.rt/](http://cloud.gretchen.rt/).

## 3. Configuration de base de Nextcloud
Maintenant que notre Nextcloud est accésible, nous allons le configurer.

Lors de la première visite, Nextcloud vous demande de créer un utilisateur d'administration, Créez-le et cliquez sur "*Install*".

Une fois l'installation de base terminée (cela peut prendre quelques minutes si votre serveur n'est pas performant), Nextcloud nous propose d'installer les application recommandées. Dans notre cas, nous allons **passer cette étape et installer nous même les applications plus tard**. Cliquez donc sur **Annuler** en bas de la page.

>Vous arriverez donc sur votre "*Tableau de bord*" et vous verrez s'afficher une brève présentation de capacitées de Nextcloud Hub. Passez cette présentation. 

Cliquez ensuite sur votre **icone de profile** (*en haut à droite*), puis sur **Applications**.

La première configuration que nous allons faire est de connecter notre annuaire LDAP afin de pouvoir l'utiliser pour authentifier les utilisateurs. Sur le menu de gauche, cliquez sur **Applications désactivées**. Dans la liste qui s'affiche devrait apparaitre une application nommée "**LDAP user and group backend**", cliquez sur le bouton **Activer** correspondant. Faites de même pour l'application **External storage support**.

Retournez ensuite sur votre image de profil, puis cliquez sur **Paramètres**. Faites défiler le menu de gauche et cliquez sur **LDAP/AD integration**. 

Dans l'onglet serveur, entrez les paramètres suivants :
```
Hote : 10.108.32.22    Port : 389

DN Utilisateur : cn=admin,dc=gretchen,dc=rt
Mot de passe : <votre_mdp_admin>

DN de base : ou=users,dc=gretchen,dc=rt
```

Sauvegardez vos identifiants en cliquant sur **Sauvegarder les informations d'identification** et vérifiez votre configuration en cliquant sur **Tester le DN de base**. Si votre configuration est validée, vous devriez voir apparaitre "*Configuration OK*" a coté du bouton Continuer.
> **NOTE** : Il est recommandé de saisir un nom DNS pour notre hôte plutot qu'une adresse IP dans un environnement de production.

Cliquez ensuite sur **Continuer**. Nous avons ensuite la possibilitée de personaliser la recherche des utilisateurs ayant accès a notre Nextcloud. Dans la liste "*Seulement ces classes d'objets*", vérifiez que **inetOrgPerson** est bien sélectionné. Vérifiez également que "*Configuration OK*" est de nouveau affiché a coté du bouton Retour, puis **cliquez sur Continuer**.

Vous pouvez maintenant customiser les attributs de connexion de vos utilisateurs, mais nous allons ici aussi conserver les paramètres par défaut. Vous pouvez entrer un nom d'utilisateur dans le champ "*Loginname de test*" et cliquer sur **Tester les paramètres**. Si tout est correct, vous devriez voir apparaitre une notification (*coin supérieur droit de la page*) indiquant "*Utilisateur trouvé et paramètres vérifiés.*" Si tel est le cas, vous pouvez cliquer sur **Continuer**

Enfin, nous arrivons sur la page de configuration des groupes. Ici, cliquez sur **Modifier la requête LDAP** et entrez `(&(objectclass=posixGroup))` . Cliquez ensuite sur **Avancé** (*en haut à droite de l'interface*), puis sur **Paramètres du dossier**. Configurez les champs suivants tels que :
```
[tronqué]

DN racine de l'arbre groupes : ou=groups,dc=gretchen,dc=rt
Attributs de recherche des groupes : posixGroup
Association groupe-membre : memberUid

[tronqué]
```

Retournez sur l'onglet **Groupes** et vérifiez que Nextcloud obtient bien le bon nombre de groupes en cliquant sur **Vérifier les paramètres et compter les groupes**

Si tout est correct, vous pouvez désormais vous déconnecter (*icone de profil > Se déconnecter*) et essayer de vous reconnecter avec un compte provenant du LDAP.

## 4. Configuration des dossiers personnels

> Section en cours de réalisation.

## 5. Ajout et configurationdes applis principales

> Section en cours de réalisation.