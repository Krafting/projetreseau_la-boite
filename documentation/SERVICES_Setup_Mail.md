# Services - Installation et configuration des services de messagerie

Installation et configuration d'un serveur de messagerie composé d'un serveur de mail complet et d'un webmail Roundcube. Nous allons tout d'abord commencer avec l'ajout des enregistrements DNS nécéssaires. 

> Vérifiez que votre poste utilise bien les bons serveurs DNS

Sur notre serveur DNS, on se place dans le dossier `/etc` et on effectue une copie du fichier `/etc/local-dns.hosts`
```
sudo cp local-dns.hosts local-dns.hosts.bak
```
> **ATTENTION** : Cela écrasera votre copie de sauvegarde précédente (si elle existe)

On y ajoute ensuite l'enregistrements requis par le proxy
```
10.108.32.24    mail
```

Pensez à bien recharger votre service DNS, ici avec dnsmasq :
```
pkill -HUP dnsmasq 
```


## Table des matières

- [1. Préparation de l'environnement](#1-préparation-de-lenvironnement)
- [2. Installation du serveur iRedMail](#2-installation-du-serveur-iredmail)
- [3. Intégration de l'annuaire LDAP](#3-intégration-de-lannuaire-ldap)

## 1. Préparation de l'environnement
Nous allons utiliser [iRedMail](https://www.iredmail.org) pour notre service mail, et celui-ci requiert une machine virtuelle séparée pour pouvoir fonctionner correctement. Nous avons donc crée une nouvelle VM Ubuntu Server qui sera dédiée aux services de messagerie. 

De plus, nous allons ici suivre la documentation officielle de iRedMail pour l'installation et la configuration, mais en l'adaptant à notre environnement. Cette documentation repose sur les procédures d'installation et de configuration suivantes :
- [Install iRedMail on Debian or Ubuntu Linux](https://docs.iredmail.org/install.iredmail.on.debian.ubuntu.html) 
- [Integrate Microsoft Active Directory for user authentication and address book](https://docs.iredmail.org/active.directory.html) 


## 2. Installation du serveur iRedMail

On commence par définir le nom d'hote de notre machine en modifiant les deux fichiers suivants :

Dans `/etc/hostname` :
```
mail
```
Dans `/etc/hosts` :
```
# Part of file: /etc/hosts
127.0.0.1   mail.gretchen.rt mail localhost localhost.localdomain
```

Vérifiez ensuite avec `hostname -f` que le nom de domaine affiché est bien le bon, si ce n'est pas le cas, redémarrez la machine. 

Il sera ensuite nécéssaire pour la suite d'installer deux paquets supplémentaires, avec la commande `sudo apt install -y wget gzip` .

Nous allons ensuite télécharger la dernière version d'iRedMail depuis [le site officiel](https://www.iredmail.org/download.html), cliquez-droit sur le bouton "Stable" et copiez l'adresse du lien. 

> ****** 
> ***ATTENTION*** : Cette partie de la documentation contient des commandes demandant un accés super-user (root) à votre serveur. On assumera que vous effectuez les commandes en tant que root (`sudo su`)
> ******

Placez-vous dans `/root` et créez le dossier download puis placez-vous dedans avec `mkdir download && cd download`. Téléchargez ensuite la dernière version de iRedMail comme suit :
```
wget <URL_de_l'archive_.tar.gz>
```

Vous devriez obtenir un fichier simmilaire à `1.x.y.tar.gz`, que vous allez décompresser avec la commande suivante :
```
tar zxf 1.x.y.tar.gz 
```

Ceci vous extraiera les fichiers dans un dossier sous la forme `iRedMAIL-1.x.y`, déplacez-vous dedans et éxécutez l'installeur avec `bash iRedMail.sh`. 

Celui-ci commencera par installer les paquets manquant pour l'installation, puis vous affichera un message de bienvenue, que vous pouvez confimer. Ensuite, il va vous falloir configurer différentes options, listées ci-dessous dans l'ordre d'apparition de l'installeur :

1. **Default mail storage path** : `/var/vmail`
2. **Preffered web server** : [✓] Nginx
3. **Preferred backend** : [✓] OpenLDAP (*n'en sélectionner* ***AUCUN*** *autre*)
4. **LDAP suffix** : `dc=gretchen,dc=rt`
5. **MySQL ROOT password** : <*entrez ici un mot de passe d'administrateur de MySQL*>
6. **First email domain name** : `gretchen.rt`
7. **Password for the mail domain administrator** : <*entrez ici un mot de passe du compte postmaster@gretchen.rt*>
8. **Optional components** : <br> [✓] Roundcubemail <br> [✓] netdata <br> [✓] iRedAdmin <br> [✓] Fail2ban

Vérifiez ensuite que les options sont correctes et validez avec `y` . L'assistant va ensuite compléter l'installation avec les paramètres que vous avez fourni. Patientez que ce dernier termine, cela peut prendre du temps.

Une fois l'installation terminée, l'assistant vous demandera si il faut appliquer les régles de pare-feu recommandées par iRedMail, répondez oui ( **`y`** ) aux dexu questions, puis redémarrez votre machine avec `reboot`

Ouvrez maintenant l'interface de gestion du proxy (*rappel : port `81`*), connectez-vous si besoin, et accédez à **Hosts > Proxy Hosts**. Cliquez ensuite sur **Add Proxy Host**. Configurez le nouvel hote tel que :
```
Domain Names : mail.gretchen.rt

Scheme : https
Forward Hostane / IP : 10.108.32.24
Forward Port : 443

X Cache Assets
✓ Block Common Exploits
✓ Websockets Support
```

Si tout c'est bien déroulé, vous devriez pouvoir accéder à votre webmail sur [http://mail.gretchen.rt/mail](http://mail.gretchen.rt/mail) et vous connecter avec le compte `postmaster@gretchen.rt`.

## 3. Intégration de l'annuaire LDAP

Maintenant que notre serveur mail est opérationnel, nous allons l'intégrer avec notre annuaire LDAP afin que nos utilisateurs puissent se connecter directement avec leur compte d'entreprise et qu'ils n'aient pas besoin d'avoir un compte spécifique au service de messagerie. On va se baser sur le deuxiemme article lié plus haut pour cette partie.

> ****** 
> ***ATTENTION*** : Cette partie de la documentation contient des commandes demandant un accés super-user (root) à votre serveur. On assumera que vous effectuez les commandes en tant que root (`sudo su`)
> ******

### 3.1. Création de l'utilisateur de liaison

Pour commencer, il va nous falloir créer un compte simple qui nous permettra de lier notre iRedMail au LDAP. Dans notre cas, il se nommera `vmail` et sera situé à la racine (`cn=vmail,dc=gretchen,dc=rt`). 

Malheureusement, ajouter un utilisateur à la racine se révèle complexe en utilisant notre outil de gestion LAM, c'est pourquoi nous devrons utiliser un outil externe, dans notre cas [Apache Directory Studio](https://directory.apache.org/studio/). Ceci nous permettra de nous baser sur le compte admin pour la création de vmail. Ajouter donc le compte `cn=vmail,dc=gretchen,dc=rt` .

> **Note** : Ce compte servant de lien entre nos deux serveurs, il est **FORTEMENT RECOMMANDE** de le sécuriser en utilisant un mot de passe long et complexe.

Une fois ce compte ajouté, nous allons vérifier qu'il est bien configuré et qu'il a accès aux utilisateurs :
```
# ldapsearch -x -h <@IP_LDAP> -D 'cn=vmail,dc=gretchen,dc=rt' -W -b 'ou=users,dc=gretchen,dc=rt'
Enter password: <password_of_vmail>
```
Si votre compte est bien configuré, la commande devrait vous lister tout les utilisateurs de votre LDAP, si ce n'est pas le cas, **vérifiez votre configuration** car cet accès est obligatoire au bon fonctionnement de la liason.

### 3.2. Activer la connexion au LDAP pour Postfix

iRedMail utilise des paramètres spécifiques qui ne nous seront plus nécéssaires avec notre LDAP, désactivez-les avec les commandes suivantes :
```
postconf -e virtual_alias_maps=''
postconf -e sender_bcc_maps=''
postconf -e recipient_bcc_maps=''
postconf -e relay_domains=''
postconf -e relay_recipient_maps=''
postconf -e sender_dependent_relayhost_maps=''
```

Ensuite, on ajoute notre nom de domaine dans `smtpd_sasl_local_domain` et `virtual_mailbox_domains` tel que :
```
postconf -e smtpd_sasl_local_domain='gretchen.rt'
postconf -e virtual_mailbox_domains='gretchen.rt'
```

On adapte la methode de transport :
```
postconf -e transport_maps='hash:/etc/postfix/transport'
```

On va maintenant activer la recherche dans notre LDAP (*on créera les trois fichiers utilisés ici plus tard*) :
```
postconf -e smtpd_sender_login_maps='proxy:ldap:/etc/postfix/ad_sender_login_maps.cf'
postconf -e virtual_mailbox_maps='proxy:ldap:/etc/postfix/ad_virtual_mailbox_maps.cf'
postconf -e virtual_alias_maps='proxy:ldap:/etc/postfix/ad_virtual_group_maps.cf'
```

Créez/modifiez ensuite le fichier `/etc/postfix/transport` :
```
gretchen.rt dovecot
```

Aplliquez avec postmap :
```
postmap hash:/etc/postfix/transport
```

Créez le fichier `/etc/postfix/ad_sender_login_maps.cf` :
```
server_host     = <@IP_LDAP>
server_port     = 389
version         = 3
bind            = yes
start_tls       = no
bind_dn         = cn=vmail,dc=gretchen,dc=rt
bind_pw         = <password_of_vmail>
search_base     = ou=users,dc=gretchen,dc=rt
scope           = sub
query_filter    = (&(mail=%s)(objectClass=person))
result_attribute= mail
debuglevel      = 0
```

Créez le fichier `/etc/postfix/ad_virtual_mailbox_maps.cf` :
```
server_host     = <@IP_LDAP>
server_port     = 389
version         = 3
bind            = yes
start_tls       = no
bind_dn         = cn=vmail,dc=gretchen,dc=rt
bind_pw         = <password_of_vmail>
search_base     = ou=users,dc=gretchen,dc=rt
scope           = sub
query_filter    = (&(objectclass=person)(mail=%s))
result_attribute= mail
result_format   = %d/%u/Maildir/
debuglevel      = 0
```

Créez le fichier `/etc/postfix/ad_virtual_group_maps.cf` :
```
server_host     = <@IP_LDAP>
server_port     = 389
version         = 3
bind            = yes
start_tls       = no
bind_dn         = cn=vmail,dc=gretchen,dc=rt
bind_pw         = <password_of_vmail>
search_base     = ou=users,dc=gretchen,dc=rt
scope           = sub
query_filter    = (&(objectClass=group)(mail=%s))
special_result_attribute = member
leaf_result_attribute = mail
result_attribute= uid
debuglevel      = 0
```

> **Note** : Dans notre configuration, le schéma des groupes LDAP n'autorise pas l'attribut `mail`, et cela fait que les "groupes" ne peuvent pas être utilisés pour envoyer des mails a tout leurs membres. Si cela est impératif pour vous, vous devrez **modifier le schéma de posixGroup** afin d'y inclure l'arrtibut `mail`.

Enfin, on retire les paramètres de iRedAPD dans la configuration de postfix :
 
 1. Ouvrez le fichier `/etc/postfix/main.cf`
 2. Commentez les lignes `check_policy_service inet:127.0.0.1:7777`

### 3.3 Vérification de la recherche LDAP avec Postfix

On va maintenant vérifier que notre configuration est correcte est fonctionelle. On commence par tester la recherche d'un compte utilisateur :
```
postmap -q user@gretchen.rt ldap:/etc/postfix/ad_virtual_mailbox_maps.cf
```

Si tout s'est bien passé, la commande vous retournera quelquechose du type `gretchen.rt/user/Maildir/`, si ce n'est pas le cas, définissez `debuglevel = 1` dans `/etc/postfix/ad_virtual_mailbox_maps.cf`

Ensuite, on vérifie que postfix récupère bien le bon mail pour l'envoi :
```
postmap -q user@gretchen.rt ldap:/etc/postfix/ad_sender_login_maps.cf
```

Si tout s'est bien passé, la commande vous retournera quelquechose du type `user@gretchen.rt`

> **Note** : iRedAPD repose sur un schéma spécifique à iRedMail. Si votre système doit utiliser le minimum de ressources possible, il est recommandé de le désactiver. Voir l'article "[Manage iRedAPD](https://docs.iredmail.org/manage.iredapd.html)"

### 3.4 Activer l'intégration de LDAP dans Dovecot

Nous allons ensuite devoir modifier la configuration de Dovecot afin qu'il utilise notre serveur LDAP et plus le serveur local. Editez les fichier `/etc/dovecot/dovecot-ldap.conf` tel que :
```
hosts           = <@IP_LDAP>:389
ldap_version    = 3
auth_bind       = yes
dn              = cn=vmail,dc=gretchen,dc=rt
dnpass          = <password_of_vmail>
base            = ou=users,dc=gretchen,dc=rt
scope           = subtree
deref           = never

# Below two are required by command 'doveadm mailbox ...'
iterate_attrs   = mail=user
iterate_filter  = (&(mail=*)(objectClass=person))

user_filter     = (&(mail=%u)(objectClass=person))
pass_filter     = (&(mail=%u)(objectClass=person))
pass_attrs      = userPassword=password
default_pass_scheme = CRYPT
user_attrs      = mail=master_user,mail=user,=home=/var/vmail/vmail1/%Ld/%Ln/,=mail=maildir:~/Maildir/
```

Redémmarez ensuite le service dovecot (`systemctl restart dovecot`).

Vérifions maintenant que la configuration est fionctionelle :
```
# telnet localhost 143                     # <- Tapez cette commande
* OK [...] Dovecot ready.

. login user@gretchen.rt password_user  # <- Tapez cette commande, n'oubliez pas le .
. OK [...] Logged in

^]                                         # <- Quittez avec "Ctrl+]", puis tapez 'quit'.
```

Si lors de la deuxiemme commande, Dovecot vous retourne "`Logged In`", c'est que la connexion est fonctionelle. Si ce n'est pas le cas, **vérifiez votre configuration**.

> **Note** :La documentation de iRedMail ne semble pas a jour pour ce qui est de l'intégration du carnet d'adresse LDAP dans Roundcube, nous allons donc passer cette partie. Ceci n'empechera pas vos utilisateur du LDAP de se connecter et d'envoyer/recevoir des mails via roundcube, mais simplement que l'autocomplétion des adresses ne sera pas aussi efficace qu'elle pourrait l'être.

Une fois la configuration modifié, **REDEMARREZ VOTRE MACHINE** afin d'etre sur que toutes les configuration sont correctes et appliquées au démarrage du serveur.

*****

Si tout est correct, vous devriez pouvoir vous connecter au webmail avec un utilisateur du LDAP
