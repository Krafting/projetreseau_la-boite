# Services - Installation de l'environnement NGINX

## Table des matières

- [1. Préparation de la machine](#1-préparation-de-la-machine)
- [2. Installation et configuration du proxy nginX](#2-installation-et-configuration-du-proxy-nginX)
- [3. Ajout de l'outil de gestion Portainer](#3-ajout-de-loutil-de-gestion-portainer)
- [4. Restauration de la console Cockpit](#4-restauration-de-la-console-cockpit)
- [5. Conclusion](#5-conclusion)

## 1. Préparation de la machine

Afin d'installer l'environnement Docker avec un proxy nginX, nous devons effectuer certains changements suceptibles d'avoir des seffets indésirables.<br>
Dans ce cas, il est **FORTEMENT RECOMMANDE** de faire des instantanés de votre système quand cela est possible.

Commencons par retirer les paquets pouvant entrer en conflit avec notre configuration.
> Dans notre cas, l'accès VPN de l'Université limitant les ports que nous pouvons utiliser, nous allons **reconfigurer** Cockpit et **désactiver** le service nginX. Si vous n'avez pas de restriction de port, et que Cockpit n'utilise pas les ports 80,81 et/ou 443, vous n'aurez pas besoin de le reconfigurer.

```
sudo systemctl stop nginx && sudo systemctl disable nginx
```
***Reconfiguration de Cockpit***

On supprime le fichier permettant de rediriger Cockpit sur le port 443 et on recharge le service :
```
sudo rm /etc/systemd/system/cockpit.socket.d/listen.conf
sudo systemctl restart cockpit
```
> **Note**: Un redémmarage complet du serveur peut être requis si la commande `systemctl restart cockpit` retourne une erreur de dépendance.

Installons ensuite les paquets nécessaires pour Docker :

```
sudo apt install -y docker.io docker-compose
```

Ensuite, afin de mieux organiser notre stockage de données en lien avec Docker, nous allons créer un dossier `/docker-data` qui contiendra les volumes montés dans nos conteneurs.

```
sudo mkdir /docker-data
```

## 2. Installation et configuration du proxy nginX

Ensuite, nous allons procéder à l'installation du proxy. Cela est fait en premier car tous nos services disponibles sur le web devront être proxiés, y compris les outils d'aministration, même si cela ne seront accésibles uniquement depuis le LAN.

Commencons par créer les dossiers de notre conteneur :

```
sudo mkdir /docker-data/nginxproxymanager /docker-data/nginxproxymanager/data /docker-data/nginxproxymanager/letsencrypt
```

Déplacez-vous ensuite dans `/docker-data/nginxproxymanager` et insérer le contenu suivant dans un nouveau fichier `docker-compose.yml` :
```
version: '3'
services:
  app:
    image: 'jc21/nginx-proxy-manager:latest'
    restart: unless-stopped
    ports:
      - '80:80'
      - '81:81'
      - '443:443'
    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
```

Composez ensuite votre stack avec `docker-compose up -d`

Une fois le conteneur en ligne, accédez à l'interface de gestion disponible sur le port 81.
> **Attention**, les identifiants par défaut sont `admin@example.com / changeme`. L'assistant de configuration vous obligera à les changer.

Notre serveur proxy est désormais en ligne.

## 3. Ajout de l'outil de gestion Portainer

Nous allons ensuite installer Portainer afin de gérer plus facillement nos conteneurs.

>Comme portainer ne fonctionne pas sur la base de dossiers montés en tant que volumes, nous n'allons pas créer de sous-dossier correspondant dans `/docker-data`

Commencons par créer le volume de portainer :
```
docker volume create portainer_data
```

On peut ensuite déployer le conteneur avec la commande suivante :
```
docker run -d -p 8000:8000 -p 9443:9443 --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce:2.11.1
```

Ensuite, nous allons configurer le proxy afin de pouvoir accéder à Portainer. Cela **requiert un enregistrement DNS**, ici `admin.web.gretchen.rt` configuré. Dans notre cas, nous allons les configurer immédiatement :

> Vérifiez que votre poste utilise bien les bons serveurs DNS

Sur notre serveur DNS, on se place dans le dossier `/etc` et on effectue une copie du fichier `/etc/local-dns.hosts`
```
sudo cp local-dns.hosts local-dns.hosts.bak
```
> **ATTENTION** : Cela écrasera votre copie de sauvegarde précédente (si elle existe)

On y ajoute ensuite les enregistrements requis par le proxy
```
10.108.32.23    others web admin
```

Pensez à bien recharger votre service DNS, ici avec dnsmasq :
```
pkill -HUP dnsmasq 
```

Ouvrez l'interface de gestion du proxy (*rappel : port `81`*), connectez-vous si besoin, et accédez à **Hosts > Proxy Hosts**. Cliquez ensuite sur **Add Proxy Host**. Configurez le nouvel hote tel que :
```
Domain Names : portainer.gretchen.rt

Scheme : https
Forward Hostane / IP : 10.108.32.23
Forward Port : 9443

X Cache Assets
✓ Block Common Exploits
X Websockets Support
```

Validez en cliquant sur **Save**.

> **Note**: Pour l'instant, nous n'avons pas d'infrastructure PKI ou de CA permettant d'emmettre un certificat pour le proxy, c'est pourquoi nous passerons en http uniquement pour l'instant.

Vous pouvez ensuite accéder à Portainer sur [http://portainer.gretchen.rt/](http://portainer.gretchen.rt/). 

Lors de son premier démarrage, Portainer demande de configurer un utilisateur Administrateur. Remplissez les champs et validez avec **Create user**.

Une fois cela fait, Portainer vous demande de choisir entre l'environnement Docker Local, ou si il doit se connecter à un environnement distant. Choisissez l'environnement local en cliquant sur **Get Started**.

Portainer est désormais configuré et disponible.

## 4. Restauration de la console Cockpit

Comme notre console Cockpit écoutait sur un port utilisé par le proxy, nous avons du la désinstaller. Nous allons donc la réinstaller et configurer le proxy.

On ajoute l'enregistrement `cockpit-web` dans notre configuration DNS:
```
10.108.32.23    others web admin cockpit-web
```

Pensez à bien recharger votre service DNS, ici avec dnsmasq :
```
pkill -HUP dnsmasq 
```

Ouvrez l'interface de gestion du proxy (*rappel : port `81`*), connectez-vous si besoin, et accédez à **Hosts > Proxy Hosts**. Cliquez ensuite sur **Add Proxy Host**. Configurez le nouvel hote tel que :
```
Domain Names : cockpit-web.gretchen.rt

Scheme : https
Forward Hostane / IP : 10.108.32.23
Forward Port : 9090

X Cache Assets
✓ Block Common Exploits
X Websockets Support
```

Validez en cliquant sur **Save**.

> **Rappel**: Pour l'instant, nous n'avons pas d'infrastructure PKI ou de CA permettant d'emmettre un certificat pour le proxy, c'est pourquoi nous passerons en http uniquement pour l'instant.

Vous pouvez ensuite accéder à Cockpit sur [http://cockpit-web.gretchen.rt/](http://cockpit-web.gretchen.rt/). 

## 5. Conclusion

Vous avez configuré votre machine afin qu'elle puisse acueillir des conteneurs Docker, configuré un environnement de gestion de ces derniers avec Portainer et vous avez également un serveur Proxy afin de gérer vos services web.